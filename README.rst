LINGRA
======

LINGRA in Python bypassed by RS data in many places @EUPL


.. inclusion-marker-do-not-remove

How to use Lingra software
==========================

You need to install:
 - Python (> 3.10) 
 - Numerical Python *(pip install numpy)*
 - Pandas

:Then Run:

.. code-block:: BASH

  bash run_lingra.sh

:Output:

A set of graphs will be written to a PNG (`lingra.png`) from the run.

.. image:: ../../lingra.png
  :width: 800


:Code:

.. literalinclude:: ../../run_lingra.sh
  :language: BASH

How to use Lingra as a function
===============================

The function `lingra()` is found in `lib_lingra.py`.

You can feed numpy arrays directly to the function, see how it is done in `run_lingra.py`

.. literalinclude:: ../../run_lingra.py
  :language: Python

How to contribute
=================

Please contact the Agri4Cast_ team

.. _Agri4Cast: eu-agri4cast-info@ec.europa.eu

JRC, Ispra, Italy

Other related pasture models
============================
- PaSim [INRAE, Clermont]
- MODVEGE [https://code.europa.eu/agri4cast/modvege]
- LINGRA [https://models.pps.wur.nl/lingra-n-grass-model]

