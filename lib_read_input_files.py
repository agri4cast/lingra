# Requirements: numpy, pandas

def lib_read_weather_file(filename):
    """
    Read the weather file and extract arrays

    :Description:
    .. csv-table::
        :file: docs/source/weather_example.csv
        :widths: 25, 50, 25
        :header-rows: 1

    :param filename:    The name of the weather.csv file
    :result:    return(year, doy, rdd, tmmn, tmmx, vp, wn, rain, RSevp, RStrn, RSlai, RScut)
    """
    import numpy as np
    import pandas as pd
    df = pd.read_csv(filename,sep=',')
    year = np.asarray((df[df.columns[0]]), dtype=int)    # year in weather file
    doy  = np.asarray((df[df.columns[1]]), dtype=int)    # doy of the year
    rdd  = np.asarray((df[df.columns[2]]), dtype=int)  # solar radiation (kj m-2 day-1)
    tmmn = np.asarray((df[df.columns[3]]), dtype=float)  # minimum temperature (degrees celsius)
    tmmx = np.asarray((df[df.columns[4]]), dtype=float)  # maximum temperature (degrees celsius)
    vp   = np.asarray((df[df.columns[5]]), dtype=float)  # water vapour pressure (kpa)
    wn   = np.asarray((df[df.columns[6]]), dtype=float)  # average wind speed (m s-1)
    rain = np.asarray((df[df.columns[7]]), dtype=float)  # daily rainfall (mm day-1)
    RSevp= np.asarray((df[df.columns[8]]), dtype=float)  # daily RS Evaporation actual (mm day-1)
    RStrn= np.asarray((df[df.columns[9]]), dtype=float)  # daily RS Transpiration actual (mm day-1)
    RSlai= np.asarray((df[df.columns[10]]),dtype=float)  # daily RS LAI (-)
    RScut= np.asarray((df[df.columns[11]]),dtype=float)  # daily RS cutting event (0/1)

    print(year[0],doy[0],rdd[0],tmmn[0],tmmx[0],vp[0],wn[0],rain[0],RSevp[0],RStrn[0],RSlai[0],RScut[0])
    return(year, doy, rdd, tmmn, tmmx, vp, wn, rain, RSevp, RStrn, RSlai, RScut)



def lib_read_params_file(filename):
    """
    Read input parameters csv file

    :Description:

    .. csv-table::
        :file: docs/source/params_example.csv
        :widths: 25, 15, 45, 15 
        :header-rows: 1

    :param filename:    The name of the params.csv file
    :result:    return([rootdi, LAIi, tilli, wrei, wrti, co2a, kdif, LAIcr, tbase, luemax, sla, cLAI, nitmax, nitr, rdrd, tmbas1, drate, irrigf, rootdm, rrdmax, wcad, wcwp, wcfc, wci, wcwet, wcst])

    """
    import numpy as np
    import pandas as pd
    df = pd.read_csv(filename,sep=',',header=None)

    #initial
    rootdi  = df[(df[0]=='rootdi')][1].values[0]  # was 0.4 
    LAIi    = df[(df[0]=='LAIi')][1].values[0]    # was 0.1  
    tilli   = df[(df[0]=='tilli')][1].values[0]   # was 7000.
    wrei    = df[(df[0]=='wrei')][1].values[0]    # was 200.
    wrti    = df[(df[0]=='wrti')][1].values[0]    # was 4.

    #************************************************************************
    #***   functions and parameters for grass
    #************************************************************************
  
    #* parameters
    co2a   = df[(df[0]=='co2a')][1].values[0]     # was 360.     # atmospheric co2 concentration (ppm)
    kdif   = df[(df[0]=='kdif')][1].values[0]     # was 0.60     # 
    LAIcr  = df[(df[0]=='LAIcr')][1].values[0]    # was 4.       # critical LAI 
    tbase  = df[(df[0]=='tbase')][1].values[0]    # was 0.        
    luemax = df[(df[0]=='luemax')][1].values[0]   # was 3.0      # light use efficiency (g dm mj-1 par intercepted)
    sla    = df[(df[0]=='sla')][1].values[0]      # was 0.0025   # specific leaf area (m2 g-1 dm)
    cLAI   = df[(df[0]=='cLAI')][1].values[0]     # was 0.8      # LAI after cutting (-)
    nitmax = df[(df[0]=='nitmax')][1].values[0]   # was 3.34     # maximum nitrogen content (%)
    nitr   = df[(df[0]=='nitr')][1].values[0]     # was 3.34     # actual nitrogen content (%)
    rdrd   = df[(df[0]=='rdrd')][1].values[0]     # was 0.01     # base death rate (fraction)
    tmbas1 = df[(df[0]=='tmbas1')][1].values[0]   # was 3.       # base temperature perennial ryegrass (degrees celsius)

    #* parameters for water relations from lingra for thimothee
    drate  = df[(df[0]=='drate')][1].values[0]    # was 50.      # drainage rate (mm day-1)
    irrigf = df[(df[0]=='irrigf')][1].values[0]   # was 0.       # irrigation (0 = no irrigation till 1 = full irrigation)
    rootdm = df[(df[0]=='rootdm')][1].values[0]   # was 0.4      # maximum root depth (m)
    rrdmax = df[(df[0]=='rrdmax')][1].values[0]   # was 0.012    # maximum root growth (m day-1)
    wcad   = df[(df[0]=='wcad')][1].values[0]     # was 0.005    # air dry water content (fraction) 
    wcwp   = df[(df[0]=='wcwp')][1].values[0]     # was 0.12     # wilting point water content (fraction) 
    wcfc   = df[(df[0]=='wcfc')][1].values[0]     # was 0.29     # field capacity water content (fraction)
    wci    = df[(df[0]=='wci')][1].values[0]      # was 0.29     # initial water content (fraction)
    wcwet  = df[(df[0]=='wcwet')][1].values[0]    # was 0.37     # minimum water content at water logging (fraction) 
    wcst   = df[(df[0]=='wcst')][1].values[0]     # was 0.41     # saturation water content (fraction)

    return([rootdi, LAIi, tilli, wrei, wrti, co2a, kdif, LAIcr, tbase, luemax, sla, cLAI, nitmax, nitr, rdrd, tmbas1, drate, irrigf, rootdm, rrdmax, wcad, wcwp, wcfc, wci, wcwet, wcst])
