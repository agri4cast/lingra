
def plot(time, LAI, tiller, yielD, wlvg, wlvd1, parcu, grass, tracu, evacu):
    """
    Plotting function, by default writes to lingra.png

    :Description:

    .. csv-table:: 
        :file: docs/source/plot_input_example.csv
        :widths: 25, 50, 25 
        :header-rows: 1

    :param time:    The array holding time
    :param LAI:     The array holding leaf area index (LAI)
    :param tiller:  The array holding the tiller number
    :param yielD:   The array holding the harvestable part of total above ground dry weight and previous harvests, kg ha-1
    :param wlvg:    The array holding the dry weight of green leaves, kg ha-1 
    :param wlvd1:   The array holding the dry weight of dead leaves, kg ha-1 
    :param parcu:   The array holding the cumulative intercepted par, mj par intercepted m-2 ha-1 
    :param grass:   The array holding the dry weight of cut green leaves, kg ha-1 
    :param tracu:   The array holding the cumulative transpiration
    :param evacu:   The array holding the cumulative evaporation
    :result:        The PNG file of the plot (i.e. lingra.png)

    """
    # PLOT the graphs
    import numpy as np
    import matplotlib.pyplot as plt

    plt.figure(figsize=(15,7))

    plt.subplot(231)
    plt.plot(time, np.divide(yielD[:len(time)],1000),'g-')
    plt.title('Yield (T/ha)')
    plt.grid()

    plt.subplot(232)
    plt.plot(time, tracu[:len(time)],'b-')
    plt.title('Transpiration accumulated (mm)')
    plt.grid()

    plt.subplot(236)
    plt.plot(time, np.divide(tiller[:len(time)],100),'r-')
    plt.title('Tiller (M#/ha)')
    plt.grid()

    plt.subplot(234)
    plt.plot(time, LAI[:len(time)],'g-')
    plt.title('LAI (-)')
    plt.grid()

    plt.subplot(235)
    plt.plot(time, evacu[:len(time)],'b-')
    plt.title('Evaporation accumulated (mm)')
    plt.grid()

    plt.subplot(233)
    plt.plot(time, np.divide(grass[:len(time)],1000),'r-')
    plt.title('Grass accumulated (T/ha)')
    plt.grid()

    plt.tight_layout()
    plt.savefig('lingra.png')
    # Uncomment if you need to show the plot
    #plt.show()

