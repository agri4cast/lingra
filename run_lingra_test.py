#Parse input values
# import argparse
# parser = argparse.ArgumentParser()
# parser.add_argument("latitude", help="Latitude in d.ddd")
# parser.add_argument("weatherfile", help="Name of the weather csv file")
# parser.add_argument("paramsfile", help="Name of the parameters csv file")
# args = parser.parse_args()

# lat  = float(args.latitude) # latitude (used to calculate daylength;
#                             # daylength not taken into account in 
#                             # lingra, latitude does not affect yield)

lat  = 52.0

sitename    = 'NL'
weatherfile = 'test.csv'
paramsfile  = 'params.csv'
outfile     = 'lingra_out.csv'

# Acquire weather and RS data
from lib_read_input_files import lib_read_weather_file
year, doy, rdd, tmmn, tmmx, vp, wn, rain, RSevp, RStrn, RSlai, RScut = \
        lib_read_weather_file(weatherfile)

# Acquire input parameters
from lib_read_input_files import lib_read_params_file
p = lib_read_params_file(paramsfile)

# Process LINGRA
from lib_lingra import lingra
test = lingra(lat, year, doy, rdd, tmmn, tmmx, vp, wn, rain, RSevp, RStrn, RSlai, RScut, p)
time, LAI, tiller, yielD, wlvg, wlvd1, parcu, grass, tracu, evacu = \
        lingra(lat, year, doy, rdd, tmmn, tmmx, vp, wn, rain, RSevp, RStrn, RSlai, RScut, p)



# Plot to lingra.png
from lib_write_output_files import plot
out = plot(time, LAI, tiller, yielD, wlvg, wlvd1, parcu, grass, tracu, evacu)

# data processing
#waterinput = raincu + irrcu  
#wateroutput = tracu + evacu + intLAI + dracu + runcu    

# output in table and .csv file
outfile = str(year[-1])+'_'+str(time[-1])+'_'+sitename+'_'+outfile
f = open(outfile, "w")
f.write("year, time, LAI, tiller, yield, wlvg, wlvd1, parcu, grass, tracu, evacu\n")
for t in range(len(time)):
    f.write("%d,%d,%.2f,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n" % (year[t], time[t], LAI[t], tiller[t], yielD[t], wlvg[t], wlvd1[t], parcu[t], grass[t], tracu[t], evacu[t]))
f.close()

