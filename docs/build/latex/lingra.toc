\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}How to use Lingra software}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}How to use Lingra as a function}{3}{chapter.2}%
\contentsline {chapter}{\numberline {3}How to contribute}{5}{chapter.3}%
\contentsline {chapter}{\numberline {4}Other related pasture models}{7}{chapter.4}%
\contentsline {chapter}{\numberline {5}Lingrs}{9}{chapter.5}%
\contentsline {section}{\numberline {5.1}lib\_read\_input\_files module}{9}{section.5.1}%
\contentsline {section}{\numberline {5.2}lib\_write\_output\_files module}{11}{section.5.2}%
\contentsline {section}{\numberline {5.3}lib\_lingra module}{12}{section.5.3}%
\contentsline {chapter}{\numberline {6}Indices and tables}{15}{chapter.6}%
\contentsline {chapter}{Python Module Index}{17}{section*.7}%
\contentsline {chapter}{Index}{19}{section*.8}%
